# uNESsential

## 0. Contents

  1. Introduction
  2. What's new
  3. Instructions
  4. Requirements
  5. Emulation
  6. Misc stuff
  7. Credits
  8. Disclaimer
  9. More stuff


## 1. Introduction

uNESsential is a NES emulator written in QBasic. It is written by
Johannes Holmberg, and includes some code by Björn "_Bnu" Åström.

## 2. What's new

### Version 0.30

 - All official instructions supported
 - PPU mirroring
 - Better palette
 - Mid-screen scrolling
 - Sound
 - Huge speedups
 - Better timing
 - Debugger works again
 - Bugfixes

## 3. Instructions

### Running uNESsential

There are two ways to run uNESsential. The recommended way is to
provide a ROM filename on the command line:

    ./uness rom.nes

Note that for ancient technical QBasicy reasons, if you provide a
filename that does not exist, uNESsential will create that file and
then crash. This is of course ridiculous but doing anything about it
is just way too much work.

You can also run it with no options:

    ./uness

This will bring up the file requester. This is only recommended in the
DOS version as the file requester does not play nicely with modern
operating systems.

### Joypad keys

    Arrows - Directional pad
    z      - A
    x      - B
    c      - Select
    v      - Start

(No, it does not make any sense!)

### Other keys

    q       - Quit
    o, i    - Toggle instruction output (debug mode)
    s       - Single-step through instructions in debug mode
    r       - Run as fast as possible in debug mode
    p       - Toggle palette emulation
    b       - Set breakpoint
    +       - Increase frameskip
    -       - Decrease frameskip
    R       - Reset emulation

Joypad emulation works on a toggle basis. Don't keep the physical
buttons held down, it will only cause misery. A pixel will light up on
the screen (under the Joypads headline) when a button is switched on.
To set a breakpoint, press b and then use the keys 1, 2, 3, and 4 to
choose a breakpoint and hit enter.

## 4. Requirements

The Linux, Mac, and Windows builds are compiled with QB64 and should
run just fine on any reasonably modern machine. The DOS version is
compiled with QuickBasic 4.5 and will be excruciatingly slow
regardless of your hardware.

## 5. Emulation

### What it emulates

 - All of the 6502 instructions (I think (for real this time))
 - Most of the PPU
 - The square and triangle sound channels
 - Joypad 1

### What it will emulate in the future:

  - Joypad 2 (possibly 3 and 4 too but probably not)
  - Some mappers maybe (but probably not)

## 6. Misc stuff

### Other things you should know

uNESsential needs to run for one second to figure out your computer
speed and how to compensate for it. During this time, it will run as
fast as it can and sound output is disabled. This means that when the
framerate limiter kicks in and sound is enabled again you may have
missed part of the title screen music. Consider resetting the emulated
NES (by pressing shift-R) if you want to hear the whole thing.

The DOS version is extremely slow. If a game seems to get stuck on the
title screen, give it some time, go eat lunch or something. Some games
need more than 100 frames to actually get into the game. (Usually
because they want to play some music.)

Sprite flipping does not currently work in the y-axis with 8x16
sprites.  I don't know of any game which uses it which means that any
implementation of it will be terribly buggy. (I don't know if this is
still true, but I decided to keep this notice in the readme rather
than figure that out.)

A few words on the palette emulation. uNESsential runs in a 16 colour
video mode. This means that good palette emulation is
impossible. Palette emulation currently works like this: Background
palette is prioritised and will get the correct colours, effectively
using up the entire VGA palette. Sprites have to pick the closest
colours possible from the background palette.

Starting with uNESsential 0.30, palette emulation is enabled by
default, which means that when emulation starts, the entire screen
will go grey until the game has set up its palettes. In DOS, this
can take a while. You can still press p to disable palette
emulation.

### Compatibility list

As of v0.30, most (mapper #0) games work fine.

### Why uNESs?

The extra 's' is because I don't want people to confuse uNESsential
with another emulator called Ultimate NES (uNES).

## 7. Credits

Thanks to the following people who made this possible:

 - Marat Fayzullin and Yoshi for their invaluable NES docs.
 - Loopy for making LoopyNES which has helped incredibly.
 - Goroh for taking the time to answer my sprite questions.
 - Tony Young for creating Junkrom and Mouser.
 - Chris Covell for making his various NES ROMs.
 - AtLaS for thinking of the name uNESsential.
 - AkIlLa^ for pointing out errors and making suggestions, and
   implementing the zaniest features in Nes4pc just to allow me to
   debug my cpu core.
 - Whoever wrote the cart.nes rom.
 - The QB64 team for reviving QBasic in the 21st century.
 - Pontus Pihlgren for inspiring the 2022 update with his PDP-8
   emulation shenanigans.
 - Probably some more people. Email me if I forgot you.

### 8. Disclaimer

Who cares anyway?

### 9. More stuff

Feel free to email me at josk@dfupdate.se.

The latest version of uNESsential can be downloaded at:
https://dataswamp.org/~josk/unessential/

The old link works too:
http://www.emucamp.com/unessential/

Copyright 1998-2022 Johannes Holmberg
